console.log("Looping Pertama")
var flag = 0
while(flag<20){
    flag += 2;
    console.log(flag + " - I love Coding") 
}
console.log("Looping Kedua")
while(flag>0){
    console.log(flag + " - I will become a mobile developer")
    flag -= 2;
}
console.log('----------------------------------------------')
for(var angka=1; angka < 21;angka ++){
    if((angka%2)===0){
        console.log(angka + " - berkualitas")
    } else if((angka%3)===0){
        console.log(angka + " - i love coding")
    } else if((angka%2)==1){
        console.log(angka + " - santai")
    }
}
console.log('------------------------------------------------')
var e = '#';
for(var index0 = 0; index0 < 4; index0 ++){
    var d = ' ';
    for(var index1 = 0; index1 < 8; index1 ++){
        d = d+e;
    }
    console.log(d);
}
console.log('-----------------------------------------------')
for(var i=1; i<=7; i++){
    console.log("#".repeat(i));
 }
 console.log('---------------------------------------------')
 var size = 8;    
for (var y = 0; y < size; y++) {
    var line = ' ';
    for (var x = 0; x < size; x++) {
        line += (y + x + 1) % 2 ? ' ': '#';
    }
    console.log(line);
}
